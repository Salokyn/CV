<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" encoding="utf-8" />

  <xsl:template match="a">
    <a href="{@href}"><xsl:value-of select="." /></a>
  </xsl:template>

  <xsl:template match="/cv">
    <html xmlns="http://www.w3.org/1999/xhtml" lang="fr" dir="ltr">
      <head>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
        <meta http-equiv="Content-Style-Type" content="text/css" />
        <title><xsl:value-of select="@titre" /></title>
        <meta name="author" content="Nicolas GIF" />
        <meta name="description" content="CV de Nicolas GIF" />
        <meta name="keywords" content="cv, curriculum vitae, Nicolas GIF" />
        <meta name="date" content="{@date}" />
        <meta name="viewport" content="width=device-width" />
        <link rel="stylesheet" type="text/css" href="CV.css" />
      </head>

      <body>
        <div class="page">
          <xsl:apply-templates select="photo" />
          <xsl:apply-templates select="etat_civil" />
          <xsl:apply-templates select="@titre" />
          <xsl:apply-templates select="competences" />
          <xsl:apply-templates select="certifications" />
          <xsl:apply-templates select="experiences" />
          <xsl:apply-templates select="formations" />
          <xsl:apply-templates select="diplomes" />
        </div>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="etat_civil">
    <div id="{name()}">
      <div class="cadre">
        <span style="font-weight: bold;"><xsl:value-of select="prenom" /><xsl:text> </xsl:text><xsl:value-of select="nom" /></span><br />
        <xsl:value-of select="adresse" /><br />
        <xsl:value-of select="CP" /> - <xsl:value-of select="ville" /><br />
        <xsl:value-of select="pays" /><br />
        <xsl:value-of select="tel_mobile" /><br />
        <a href="mailto:{email}"><xsl:value-of select="email" /></a>
      </div>
    </div>
  </xsl:template>

  <xsl:template match="photo">
    <div id="{name()}">
      <img src="{.}" title="Photo" alt="Photo" />
    </div>
  </xsl:template>

  <xsl:template match="@titre">
    <h1><xsl:value-of select="." /></h1>
  </xsl:template>

  <xsl:template match="competences">
    <section id="{name()}">
      <h2><xsl:value-of select="@titre" /></h2>
      <div class="section">
        <ul>
          <xsl:apply-templates select="competence" />
        </ul>
      </div>
    </section>
  </xsl:template>

  <xsl:template match="experiences">
    <section id="{name()}">
      <h2><xsl:value-of select="@titre" /></h2>
      <div class="section">
        <xsl:apply-templates select="experience">
          <xsl:sort select="substring-after(du,'/')" order="descending" data-type="number" />
          <xsl:sort select="substring-before(du,'/')" order="descending" data-type="number" />
        </xsl:apply-templates>
      </div>
    </section>
  </xsl:template>

  <xsl:template match="formations">
    <section id="{name()}">
      <h2><xsl:value-of select="@titre" /></h2>
      <div class="section">
        <xsl:apply-templates select="formation">
          <xsl:sort select="date" order="descending" />
        </xsl:apply-templates>
      </div>
    </section>
  </xsl:template>

  <xsl:template match="certifications">
    <section id="{name()}">
      <h2><xsl:value-of select="@titre" /></h2>
      <div class="section">
        <xsl:apply-templates select="certification">
          <!--<xsl:sort select="date" order="descending" />-->
        </xsl:apply-templates>
      </div>
    </section>
  </xsl:template>

  <xsl:template match="diplomes">
    <section id="{name()}">
      <h2><xsl:value-of select="@titre" /></h2>
      <div class="section">
        <xsl:apply-templates select="*">
          <xsl:sort select="date" order="descending" data-type="number" />
        </xsl:apply-templates>
      </div>
    </section>
  </xsl:template>

  <xsl:template match="competence">
    <li class="libelle"><xsl:value-of select="nom" /></li>
    <xsl:apply-templates select="details" />
  </xsl:template>

  <xsl:template match="experience">
    <div class="ligne">
      <div class="gauche">
        <xsl:if test="au"><xsl:value-of select="du" /><xsl:text> </xsl:text><xsl:value-of select="/cv/traductions/traduction[@id='à']" /><xsl:text> </xsl:text><xsl:value-of select="au" /></xsl:if>
        <xsl:if test="not(au)"><xsl:value-of select="/cv/traductions/traduction[@id='Depuis']" /><xsl:text> </xsl:text><xsl:value-of select="du" /></xsl:if>
      </div>
      <div class="droite">
        <span class="libelle"><xsl:value-of select="poste" /></span><br />
        <xsl:apply-templates select="etablissement" />
        <div class="detail">
          <xsl:if test="objectif"><p><span style="font-weight: bold;"><xsl:value-of select="/cv/traductions/traduction[@id='Objectif']" /></span> : <xsl:value-of select="objectif" /></p></xsl:if>
          <xsl:apply-templates select="details" />
          <xsl:if test="technologies"><p><span style="font-weight: bold;">Technologies</span> : <xsl:value-of select="technologies" /></p></xsl:if>
        </div> <!-- detail -->
      </div> <!-- droite -->
    </div> <!-- ligne -->
  </xsl:template>

  <xsl:template match="diplome">
    <div class="ligne">
      <div class="gauche"><xsl:value-of select="date" /></div>
      <div class="droite">
        <span class="libelle"><xsl:value-of select="nom" /></span><xsl:if test="specialite"> - <xsl:value-of select="specialite" /></xsl:if><br />
        <xsl:apply-templates select="etablissement" />
        <xsl:if test="mention"><span class="detail">mention <xsl:value-of select="mention" /></span></xsl:if>
      </div> <!-- droite -->
    </div> <!-- ligne -->
  </xsl:template>

  <xsl:template match="certification">
    <div class="ligne">
      <div class="gauche">
        <xsl:value-of select="date" />
      </div>
      <div class="droite">
        <span class="libelle"><xsl:value-of select="nom" /></span><br />
        <span class="detail"><xsl:apply-templates select="detail" /></span>
      </div> <!-- droite -->
    </div> <!-- ligne -->
  </xsl:template>

  <xsl:template match="formation">
    <div class="ligne">
      <div class="gauche">
        <xsl:value-of select="date" />
        <xsl:if test="duree"><br/><xsl:value-of select="duree" /></xsl:if>
      </div>
      <div class="droite">
        <span class="libelle"><xsl:value-of select="nom" /></span><br />
        <xsl:apply-templates select="etablissement" />
      </div> <!-- droite -->
    </div> <!-- ligne -->
  </xsl:template>

  <xsl:template match="details">
    <ul>
      <xsl:for-each select="detail">
        <li><xsl:value-of select="." /></li>
      </xsl:for-each>
    </ul>
  </xsl:template>

  <xsl:template match="etablissement">
    <xsl:param name="id" select="@id" />
    <xsl:variable name="etablissement" select="/cv/etablissements/etablissement[@id = $id]" />
    <p>
      <xsl:if test="$etablissement/web"><a href="{$etablissement/web}"><xsl:value-of select="$etablissement/nom" /></a></xsl:if>
      <xsl:if test="not($etablissement/web)"><xsl:value-of select="$etablissement/nom" /></xsl:if>
      - <xsl:value-of select="$etablissement/ville" />
    </p>
  </xsl:template>

</xsl:stylesheet>
